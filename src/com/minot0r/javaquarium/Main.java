package com.minot0r.javaquarium;

import com.minot0r.javaquarium.core.Aquarium;

import java.util.Random;

/**
 * Créé par @Valentin le 18/02/2018.
 */
public class Main {

    private static Random randomizer = new Random();

    public static void main(String[] args){
        Aquarium aquarium = new Aquarium();
        aquarium.startGame();
    }

    public static int getRandomNumber(int max){
        return Main.randomizer.nextInt(max);
    }

    public static void log(Object log){
        System.out.println(log);
    }

}
