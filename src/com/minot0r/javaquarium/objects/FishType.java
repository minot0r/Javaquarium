package com.minot0r.javaquarium.objects;

/**
 * Créé par @Valentin le 18/02/2018.
 */
public enum FishType {
    Merou(true, "Merou"),
    Thon(true, "Thon"),
    Clown(true, "Poisson-Clown"),
    Sole(false, "Sole"),
    Bar(false, "Bar"),
    Carpe(false, "Carpe");

    private boolean carnivore;
    private String name;

    FishType(boolean carnivore, String name) {
        this.carnivore = carnivore;
        this.name = name;
    }

    public boolean isCarnivore(){
        return this.carnivore;
    }

    public String getRace(){
        return this.name;
    }

    public static FishType fromName(String name){
        switch(name) {
            case "Merou":
                return FishType.Merou;
            case "Thon":
                return FishType.Thon;
            case "Poisson-Clown":
                return FishType.Clown;
            case "Sole":
                return FishType.Sole;
            case "Bar":
                return FishType.Bar;
            case "Carpe":
                return FishType.Carpe;
            default:
                return null;
        }
    }
}
