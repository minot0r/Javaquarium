package com.minot0r.javaquarium.objects;

/**
 * Créé par @Valentin le 18/02/2018.
 */
public enum Sex {
    Opportunist("Tout"),
    Male("Male"),
    Female("Femelle");

    private String sex;

    Sex(String sex) {
        this.sex = sex;
    }

    public String getName(){
        return this.sex;
    }

    public static Sex fromName(String name){
        switch(name){
            case "Tout":
                return Sex.Opportunist;
            case "Male":
                return Sex.Male;
            case "Femelle":
                return Sex.Female;
            default:
                return null;
        }
    }
}
