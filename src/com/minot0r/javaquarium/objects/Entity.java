package com.minot0r.javaquarium.objects;

/**
 * Créé par @Valentin le 18/02/2018.
 */
public abstract class Entity {

    protected int lifePoints;
    protected int oldness;

    public Entity(int lifePoints){
        this.lifePoints = lifePoints;
        this.oldness = 0;
    }

    abstract EntityType getEntityType();

    public int getLifePoints(){
        return this.lifePoints;
    }

    public void hitEntity(int damage){
        this.lifePoints -= damage;
    }

    public void heal(int healing){
        this.lifePoints += healing;
    }
    
    public int getOldness(){
        return this.oldness;
    }

    public void passYear(){
        if(this.getOldness() < 20) {
            this.oldness += 1;
        }else{
            this.lifePoints = 0;
        }
    }

    public boolean isDead(){
        return lifePoints <= 0;
    }

}
