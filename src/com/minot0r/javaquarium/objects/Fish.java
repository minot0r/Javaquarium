package com.minot0r.javaquarium.objects;


import com.minot0r.javaquarium.Main;

/**
 * Créé par @Valentin le 18/02/2018.
 */
public class Fish extends Entity {

    private Sex sex;
    private FishType type;
    private String name;
    private boolean hadSex;

    public Fish(String name, FishType type, Sex sex, int lifePoints){
        super(lifePoints);
        this.name = name;
        this.type = type;
        this.sex = sex;
        this.hadSex = false;
    }

    public FishType getType(){
        return this.type;
    }

    public Sex getSex(){
        if((this.getType().equals(FishType.Bar) || this.getType().equals(FishType.Merou)) && this.getOldness() == 10){
            this.sex = Sex.Female;
        }
        return this.sex;
    }

    public String getName(){
        return this.name;
    }

    @Override
    EntityType getEntityType() {
        return EntityType.Fish;
    }

    public void sex(boolean had){
        this.hadSex = had;
    }

    public boolean hadSex() {
        return this.hadSex;
    }

    @Override
    public void passYear() {
        super.passYear();
        this.hadSex = false;
    }

    public Fish setOldness(int age){
        this.oldness = age;
        return this;
    }
}
