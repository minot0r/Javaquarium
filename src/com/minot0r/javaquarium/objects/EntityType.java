package com.minot0r.javaquarium.objects;

/**
 * Créé par @Valentin le 18/02/2018.
 */

public enum EntityType {
    SeaWeed("Algue"),
    Fish("Poisson");

    private String entityName;

    EntityType(String entityName) {
        this.entityName = entityName;
    }

    String getName(){
        return this.entityName;
    }
}
