package com.minot0r.javaquarium.objects;

/**
 * Créé par @Valentin le 18/02/2018.
 */
public class SeaWeed extends Entity {

    public SeaWeed(int lifePoints){
        super(lifePoints);
    }

    @Override
    EntityType getEntityType() {
        return EntityType.SeaWeed;
    }

    public void setAge(int age){
        this.oldness = age;
    }

    public SeaWeed setOldness(int age){
        this.oldness = age;
        return this;
    }

}
