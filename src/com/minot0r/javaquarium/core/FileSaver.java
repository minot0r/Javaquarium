package com.minot0r.javaquarium.core;

import com.minot0r.javaquarium.objects.Entity;
import com.minot0r.javaquarium.objects.Fish;
import com.minot0r.javaquarium.objects.SeaWeed;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Créé par @Valentin le 23/02/2018.
 */
public class FileSaver {

    public static void saveToFile(LinkedList<Entity> entities, String path){
        try {
            File f = new File(path);
            if(f.exists()) f.delete();
            PrintWriter pw = new PrintWriter(path, "UTF-8");
            for(Entity ent : new ArrayList<>(entities)){
                if(ent instanceof SeaWeed){
                    SeaWeed sw = (SeaWeed) ent;
                    pw.write("algue " + sw.getOldness() + " " + sw.getLifePoints() + "\r\n");
                }else if(ent instanceof Fish){
                    Fish fish = (Fish) ent;
                    pw.write("poisson " + fish.getName() + " " + fish.getOldness() + " " + fish.getLifePoints()
                            + " " + fish.getType().getRace() + " " + fish.getSex().getName() + "\r\n");
                }
            }
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
