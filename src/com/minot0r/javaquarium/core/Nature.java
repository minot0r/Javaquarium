package com.minot0r.javaquarium.core;

import com.minot0r.javaquarium.Main;
import com.minot0r.javaquarium.objects.Entity;
import com.minot0r.javaquarium.objects.Fish;
import com.minot0r.javaquarium.objects.SeaWeed;
import com.minot0r.javaquarium.objects.Sex;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Créé par @Valentin le 21/02/2018.
 */
class Nature {


    static void live(LinkedList<Entity> entities){
        reproduceSeaWeeds(entities);
        for(Entity ent : new ArrayList<>(entities)){
            ent.passYear(); // Passe l'année d'une entitée
        }
        for(Entity ent : new ArrayList<>(entities)){
            if(ent instanceof Fish){
                Fish f = (Fish) ent;
                f.hitEntity(1); // Dégats de début de tour
                if(f.isDead()) {
                    Main.log("Le poisson : " + f.getName() + " est mort.");
                    entities.remove(ent);
                }else if(f.getLifePoints() <= 5) { // Si il est affamé
                    attackRandom(entities, f);
                }else if(!f.hadSex()){
                    reproduceFish(entities, f);
                }
            }else if(ent instanceof SeaWeed) {
                SeaWeed sw = (SeaWeed) ent;
                if (sw.isDead()) {
                    entities.remove(ent);
                }
                Main.log("Une algue s'est soignée de 1 point de vie.");
                sw.heal(1);
            }
        }
    }

    /*
     *  Reproduction d'un poisson
     */
    private static void reproduceFish(LinkedList<Entity> entities, Fish f) {
        Fish repo = pickReproFish(entities, f);
        if(repo == null) { return; }
        f.sex(true);
        repo.sex(true);
        entities.add(new Fish("RandomName", f.getType(), f.getSex(), 10));
        Main.log(f.getName() + " a fait un enfant avec " + repo.getName() + ".");
    }


    /*
     *   Attaque un autre poisson si le poisson est carnivore,
     *   attaque une algue si le poisson est végétarien
     */
    private static void attackRandom(LinkedList<Entity> entities, Fish fish) {
        Entity victim;
        if(fish.getType().isCarnivore()){
            victim = pickRandomFish(entities, fish);
            if(victim == null) return;

            Main.log("Le poisson " + fish.getName() + " à attaqué " + ((Fish) victim).getName() + " de 4 dégâts");
            victim.hitEntity(4);
            if(victim.isDead()) {
                fish.heal(5);
                Main.log(((Fish) victim).getName() + " est mort, " + fish.getName() + " à gagné 5 points de vie");
                entities.remove(victim);
            }
        }else{
            victim = pickRandomSeaWeed(entities);
            if(victim == null) return;

            Main.log("Le poisson " + fish.getName() + " à attaqué une algue et à gagné 3 points de vie");
            victim.hitEntity(3);
            fish.heal(3);
            if(victim.isDead()){
                entities.remove(victim);
            }

        }
    }


    private static void reproduceSeaWeeds(LinkedList<Entity> entities) {
        LinkedList<Entity> sw = (LinkedList<Entity>) entities.clone();
        for(Entity ent : new ArrayList<>(sw)){
            if(ent instanceof SeaWeed){
                if(ent.getLifePoints() > 10){
                    Main.log("Une algue s'est séparée en 2 pour donner naissance a 2 algues!");
                    entities.remove(ent);
                    SeaWeed seaweed = new SeaWeed(ent.getLifePoints()/2);
                    SeaWeed seaweed2 = new SeaWeed(ent.getLifePoints()/2);
                    seaweed.setAge(ent.getOldness());
                    seaweed2.setAge(ent.getOldness());
                    entities.add(seaweed);
                    entities.add(seaweed2);
                }
            }
        }
    }

    /*
     *   Permet d'avoir un poisson qui n'est pas de la même race que celui ci
     */
    private static Fish pickRandomFish(LinkedList<Entity> entities, Fish fish){
        LinkedList<Entity> fishes = (LinkedList<Entity>) entities.clone();
        for(Entity ent : new ArrayList<>(fishes)) {
            if (ent instanceof SeaWeed) {
                fishes.remove(ent);
            }else if(ent instanceof Fish
                    && ((Fish) ent).getType().getRace().equals(fish.getType().getRace())
                    || ent.equals(fish)) {
                fishes.remove(ent);
            }
        }
        if(fishes.isEmpty()){
            return null;
        }
        return (Fish) fishes.get(Main.getRandomNumber(fishes.size()));
    }

    /*
     *  Cherche un poisson aléatoire pour reproduction
     */
    private static Fish pickReproFish(LinkedList<Entity> entities, Fish fish){
        LinkedList<Entity> fishes = (LinkedList<Entity>) entities.clone();
        for(Entity ent : new ArrayList<>(fishes)) {
            if (ent instanceof SeaWeed) {
                fishes.remove(ent);
            }else if(ent instanceof Fish
                    && (
                        !((Fish) ent).getType().getRace().equals(fish.getType().getRace())
                        || ent.equals(fish)
                        || (((Fish) ent).getSex().equals(fish.getSex()) && !((Fish) ent).getSex().equals(Sex.Opportunist))
                        || ((Fish) ent).hadSex()
                    )
            ) {
                fishes.remove(ent);
            }
        }

        if(fishes.isEmpty()){
            return null;
        }
        return (Fish) fishes.get(Main.getRandomNumber(fishes.size()));
    }


    /*
     *   Choisi une algue random
     */
    private static SeaWeed pickRandomSeaWeed(LinkedList<Entity> entities){
        LinkedList<Entity> seaweeds = (LinkedList<Entity>) entities.clone();
        for(Entity ent : new ArrayList<>(seaweeds)){
            if(!(ent instanceof SeaWeed)){
                seaweeds.remove(ent);
            }
        }
        if(seaweeds.isEmpty()){
            return null;
        }
        return (SeaWeed) seaweeds.get(Main.getRandomNumber(seaweeds.size()));
    }

}
