package com.minot0r.javaquarium.core;

import com.minot0r.javaquarium.Main;
import com.minot0r.javaquarium.objects.*;

import java.io.*;
import java.util.LinkedList;

/**
 * Créé par @Valentin le 24/02/2018.
 */
public class FileLoader {

    public static LinkedList<Entity> loadFromFile(String path){
        LinkedList<Entity> entities = new LinkedList<>();
        try {
            String line;
            BufferedReader bufferreader = new BufferedReader(new FileReader(path));
            line = bufferreader.readLine();
            int lineIndex = 1;
            while (line != null) {
                Entity e;
                if((e = parseEntity(line)) != null) {
                    entities.add(e);
                }else{
                    Main.log("Une erreur est apparue pendant le chargement de la ligne " + lineIndex + "... Passage a la ligne suivante");
                }
                line = bufferreader.readLine();
                lineIndex++;
            }

        } catch (FileNotFoundException ex) {
            Main.log("Fichier : " + new File(path).getAbsolutePath() + " introuvable!");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return entities;
    }

    private static Entity parseEntity(String line){
        Object[] args = line.split(" ");
        if(args[0].equals("poisson") && args.length == 6) {
            if(FishType.fromName((String) args[4]) == null) return null;
            if(Sex.fromName((String) args[5]) == null) return null;
            String name = (String) args[1];
            int oldness = Integer.parseInt((String) args[2]);
            int lifePoints = Integer.parseInt((String) args[3]);
            FishType race = FishType.fromName((String) args[4]);
            Sex sex = Sex.fromName((String) args[5]);
            return new Fish(name, race, sex, lifePoints).setOldness(oldness);
        }else if(args[0].equals("algue") && args.length == 3){
            return new SeaWeed(Integer.parseInt((String) args[2])).setOldness(Integer.parseInt((String) args[1]));
        }
        return null;
    }

}
