package com.minot0r.javaquarium.core;

import com.minot0r.javaquarium.Main;
import com.minot0r.javaquarium.objects.*;

import java.util.LinkedList;
import java.util.Scanner;

/**
 * Créé par @Valentin le 18/02/2018.
 */
public class Aquarium {

    private LinkedList<Entity> entities = new LinkedList<>();
    private int round;

    public Aquarium(){
        this.entities = FileLoader.loadFromFile("config.cpoisson");
    }

    /*
        Itération de chaque entitée, puis création d'un message final résumant l'état actuel de chaque entitée.
     */
    private void getStatus(){
        StringBuilder finMess = new StringBuilder();
        for(Entity ent : this.entities){
            if(ent instanceof Fish){
                Fish f = (Fish) ent;
                finMess.append("- Poisson: \"").append(f.getName()).append("\" du sexe: ").append(f.getSex().getName()).append(", de la race: ").append(f.getType().getRace()).append(". Vie :").append(ent.getLifePoints()).append(". Age: ").append(ent.getOldness()).append(" ans.\n");
            }else if(ent instanceof SeaWeed){
                finMess.append("- Une algue. Vie : ").append(ent.getLifePoints()).append(". Age:").append(ent.getOldness()).append(" ans.\n");
            }
        }
        Main.log(finMess.toString());
    }

    /*
     *  Début de la "partie" à partir de la liste 'entities'
     */
    public void startGame() {
        this.round = 1;
        this.getStatus();
        while (this.entities.size() != 0){
            Main.log("= TOUR " + this.round + " =");
            Main.log("= ACTIONS =");
            Nature.live(entities);
            Main.log("= STATUT =");
            this.getStatus();
            this.saveRound();
            this.waitKey();
            this.round++;
        }
    }

    /*
     *  Sauvegarde du round
     */
    private void saveRound() {
        FileSaver.saveToFile(this.entities, "round" + this.round + ".poisson");
    }


    /*
     *   Attente d'une entrée de touche par l'utilisateur
     */
    private void waitKey() {
        new Scanner(System.in).nextLine();
    }

    /*
     *   Détermination de l'action pour chaque entitée
     */


}
